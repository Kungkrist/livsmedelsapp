//
//  ItemsTableViewController.m
//  Livsmedelsapp
//
//  Created by DEA on 2016-02-26.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "ItemsTableViewController.h"
#import "CustomTableViewCell.h"
#import "CompareViewController.h"

@interface ItemsTableViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *compareBarButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentController;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelBarButton;
@property () JSONHelper *jsonHelper;
@property () SingeltonFoodItemList *foodItems;
@property () UISearchController *searchController;
@property (nonatomic) NSArray *searchResult;
@property (nonatomic) NSArray *list;
@end

@implementation ItemsTableViewController {
    int itemsLoadingCounter;
    int notActiveSegmentIndex;
    BOOL compareIsActive;
}

- (IBAction)cancelBarButtonClick:(UIBarButtonItem *)sender {
    if(!compareIsActive) {
        self.tableView.editing = NO;
        [self.cancelBarButton setEnabled:YES];
        [self.cancelBarButton setTintColor: [UIColor clearColor]];
        self.cancelBarButton.enabled = NO;
    }else {
        compareIsActive = NO;
        self.compareBarButton.tintColor = [UIColor blueColor];
        [self.cancelBarButton setEnabled:NO];
        [self.cancelBarButton setTintColor: [UIColor clearColor]];
        for (FoodItem *item in self.foodItems.selectedItemsForCompare) {
            item.switchIsActive = NO;
        }
        self.foodItems.selectedItemsForCompare = @[].mutableCopy;
        [self.tableView reloadData];
    }
}

- (void) save{
    NSData *encodedList = [NSKeyedArchiver archivedDataWithRootObject:self.foodItems.savedItems];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedList forKey:@"foodItemList"];
    [defaults synchronize];
    
}

- (void) load {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedList = [defaults objectForKey:@"foodItemList"];
    self.foodItems.savedItems = [NSKeyedUnarchiver unarchiveObjectWithData:encodedList];
}

-(NSArray*) searchResult {
    if(_searchResult == nil)
        _searchResult = @[];
    
    return _searchResult;
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchText = searchController.searchBar.text;
    
    NSPredicate *findItemsWithName = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    self.searchResult = [self.list filteredArrayUsingPredicate:findItemsWithName];
    
    [self.tableView reloadData];
}

-(void)populateWithCell:(CustomTableViewCell*)cell indexPathRow:(int)index result:(id)result identifier:(int)identifier {
    if(identifier == -1) {
        
    }
    if(identifier == 1) {
        itemsLoadingCounter--;
        NSDictionary *arr = result[@"nutrientValues"];
        FoodItem *item;
        
        if(self.searchResult.count > 0) {
            item = self.searchResult[index];
        }else {
            item = self.list[index];
        }
        
        item.nutrientValues = arr;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(cell.tag == index){
                cell.fatLabel.text = [NSString stringWithFormat:@"Fat: %@", arr[@"fat"]];
                cell.energyLabel.text = [NSString stringWithFormat:@"Energy: %@Kj, %@Kcal", arr[@"energyKj"], arr[@"energyKcal"]];
                cell.protainLabel.text = [NSString stringWithFormat:@"Protain: %@", arr[@"protein"]];
                
                if(itemsLoadingCounter <= 0){
                    self.segmentController.enabled = YES;
                    [self.segmentController setEnabled:YES forSegmentAtIndex:notActiveSegmentIndex];
                }
                
                if(item.image != nil) {
                    if(cell.tag == index) {
                        cell.image = item.image;
                    }
                }else {
                    cell.image = [UIImage imageNamed:@"questionmark"];
                }
                [cell stopSpinner];
            }
        });
        
    }
}

-(void)populate:(id)result identifier:(int)identifier {
    if(identifier == -1) {
        NSLog(@"Error: %@", result);
        dispatch_async(dispatch_get_main_queue(), ^{
            [[self.view viewWithTag:12] stopAnimating];
            [self showDialogWithMessage:@"Please check your internet connection." title:@"Message"];
        });
    }
    
    if(identifier == 1) {
        for(NSDictionary *d in result) {
            NSString *numberAsString = d[@"number"];
            int number = numberAsString.intValue;
            
            FoodItem *item = [[FoodItem alloc] initWithName:d[@"name"] Number:number];
            [self.foodItems addItem:item];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [[self.view viewWithTag:12] stopAnimating];
            
        });
    }
    
    if (identifier == 2) {
        NSDictionary *nutrients = result[@"nutrientValues"];
        FoodItem *item =  self.foodItems.savedItems.lastObject;
        item.nutrientValues = nutrients;
        [self save];
    }
    
    if(identifier == 3) {
        NSLog(@"result: %@", result);
    }
}

-(void)dealloc {
    [self.searchController.view removeFromSuperview];
}

- (IBAction)compareClick:(UIBarButtonItem *)sender {
    compareIsActive = !compareIsActive;
    
    if(compareIsActive) {
        self.compareBarButton.tintColor = [UIColor greenColor];
        
    }else {
        self.compareBarButton.tintColor = [UIColor blueColor];
        
    }
    
    [self.cancelBarButton setEnabled: !self.cancelBarButton.isEnabled];
    
    if([self.cancelBarButton isEnabled]){
        [self.cancelBarButton setTintColor: [UIColor blueColor]];
        
    }else {
        if(self.foodItems.selectedItemsForCompare.count >= 2 && self.foodItems.selectedItemsForCompare.count <= 5) {
            CompareViewController *myNewVC = [[CompareViewController alloc] init];
            
            [self presentViewController:myNewVC animated:YES completion:nil];
            [self.cancelBarButton setTintColor: [UIColor clearColor]];
        }else {
            [self showDialogWithMessage:@"The amount of selected items need to be between 2 - 5." title:@"Message"];
        }
    }
    
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    compareIsActive = NO;
    notActiveSegmentIndex = 1;
    itemsLoadingCounter = 0;
    self.jsonHelper = [JSONHelper getInstance];
    self.foodItems = [SingeltonFoodItemList getInstance];
    
    [self.cancelBarButton setEnabled:NO];
    [self.cancelBarButton setTintColor: [UIColor clearColor]];
    
    self.list = self.foodItems.items;
    
    [self load];
    
    if(self.list.count == 0) {
        [self startSpinner];
    }
    
    self.searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
    
    self.searchController.searchResultsUpdater = self;
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    [self.searchController loadViewIfNeeded];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     //self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void) startSpinner {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(screenWidth/2., screenHeight/2);
    spinner.tag = 12;
    [self.view addSubview:spinner];
    [spinner startAnimating];
}

-(void)viewWillAppear:(BOOL)animated {

}
- (IBAction)segmentControlClick:(UISegmentedControl *)sender {
    if(itemsLoadingCounter <= 0) {
        switch ([sender selectedSegmentIndex]) {
            case 0:
                self.list = self.foodItems.items;
                notActiveSegmentIndex = 1;
                [self.tableView reloadData];
                break;
            case 1:
                self.list = self.foodItems.savedItems;
                notActiveSegmentIndex = 0;
                [self.tableView reloadData];
                break;
            default:
                break;
        }
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        if(self.searchResult.count > 0)
            return self.searchResult.count;
        
        return self.list.count;
        
    }

    return 0;
}

-(void) setUpCell:(CustomTableViewCell*)cell item:(FoodItem*)item indexPathRow:(int)row {
    
    if(compareIsActive) {
        cell.compareSwitch.hidden = NO;
        cell.compareSwitch.enabled = YES;
        [cell.compareSwitch setOn:item.switchIsActive];
    }else {
        cell.compareSwitch.hidden = YES;
        cell.compareSwitch.enabled = NO;
    }

    cell.itemName.text = item.name;
    
    if(item.nutrientValues == 0) {
        [self.jsonHelper getJsonObjectsWithCell:cell indexPathRow:row sender:self
                                           link:[NSString stringWithFormat:@"http://matapi.se/foodstuff/%d", item.number]
                                             identifier:1];
        
        [self.segmentController setEnabled:NO forSegmentAtIndex:notActiveSegmentIndex];
        [cell startSpinner];
        
        itemsLoadingCounter++;
    }else {
        cell.itemName.text = item.name;
        cell.fatLabel.text = [NSString stringWithFormat:@"Fat: %@", item.nutrientValues[@"fat"]];
        cell.energyLabel.text = [NSString stringWithFormat:@"Energy: %@Kj, %@Kcal", item.nutrientValues[@"energyKj"], item.nutrientValues[@"energyKcal"]];
        cell.protainLabel.text = [NSString stringWithFormat:@"Protain: %@", item.nutrientValues[@"protein"]];
        if(self.segmentController.selectedSegmentIndex >= 0) {
            if(item.image != nil) {
                //NSLog(@"name: %@", item.name);
                if(cell.tag == row) {
                    //  NSLog(@"visa rowen: %ld - %d", (long)cell.tag, (int)indexPath.row);
                    cell.image = item.image;
                }else {
                    cell.image = [UIImage imageNamed:@"questionmark"];
                }
            }
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.tag = indexPath.row;
    cell.image = [UIImage imageNamed:@"questionmark"];
    
    if(self.segmentController.selectedSegmentIndex == 1) {
        
        if(self.searchResult.count <= 0) {
            FoodItem *item = self.list[indexPath.row];
            cell.itemName.text = item.name;
            [self setUpCell:cell item:item indexPathRow:(int)indexPath.row];
            
        } else {
            [self setUpCell:cell item:self.searchResult[indexPath.row] indexPathRow:(int)indexPath.row];
            
        }
    }else {
        if(self.searchResult.count <= 0) {
            cell.fatLabel.text = @"";
            cell.energyLabel.text = @"";
            cell.protainLabel.text = @"";
            
            [self setUpCell:cell item:self.list[indexPath.row] indexPathRow:(int)indexPath.row];
            
        }else {
            [self setUpCell:cell item:self.searchResult[indexPath.row] indexPathRow:(int)indexPath.row];
        }
    }
    
    return cell;
}

// Override to support conditional editing of the table view
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    if(indexPath.section == 0)
        return YES;
    
    return NO;
}



- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.cancelBarButton setEnabled:YES];
    [self.cancelBarButton setTintColor: [UIColor blueColor]];
    
    if (self.segmentController.selectedSegmentIndex == 0) {
        //if(self.searchController.isActive)
            self.tableView.editing = YES;
        return UITableViewCellEditingStyleInsert;
    } else {
        //if(self.searchController.isActive)
            self.tableView.editing = YES;
        return UITableViewCellEditingStyleDelete;
    }
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.foodItems.savedItems removeObjectAtIndex:indexPath.row];
        [self.tableView reloadData];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        NSArray *arr = self.searchResult.count > 0 ? self.searchResult : self.list;
        FoodItem *item = arr[indexPath.row];
        
        NSArray *specifiedItemArray = [self.foodItems.savedItems filteredArrayUsingPredicate:[NSPredicate
                                              predicateWithFormat:@"name == %@", item.name]];
        
        if(specifiedItemArray.count == 0) {
            [self showDialogWithMessage:@"Item is now saved!" title:@"Message"];
            [self.foodItems.savedItems addObject:item];
            
        }else {
            [self showDialogWithMessage:@"Item is already saved!" title:@"Message"];
        }
    }
    
    [self save];
}

-(void) showDialogWithMessage:(NSString *)message title:(NSString *)title {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    NSArray *arr = self.searchResult.count > 0 ? self.searchResult : self.list;
    
    if([segue.identifier  isEqual: @"ItemDetailIdentifier"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        CustomTableViewCell *cell = sender;
        ViewController *destination = [segue destinationViewController];
        destination.title = cell.itemName.text;
        self.foodItems.currentItem = arr[indexPath.row];
        
    }else if ([segue.identifier isEqualToString:@"ChangeImageIdentifier"]) {
        NSIndexPath *path = [self getButtonIndexPath:sender];
        
        FoodItem *item = arr[path.row];
        ChangeImageViewController *destination = [segue destinationViewController];
        [destination setSender:self item: item];
    } else if([segue.identifier isEqualToString:@"compareIdentifier"]) {
        
        //CompareViewController *destination = [segue destinationViewController];
        //destination.foodItems = self.foodItems.selectedItemsForCompare;
    }
}

#pragma mark - Cell Image

// This will be called when the user is trying to change the picture of a cell.
-(void)imageWillBeChanged:(UIImage *)image item:(FoodItem *)item{
    item.image = image;
    [self.tableView reloadData];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths[0] stringByAppendingString:[NSString stringWithFormat:@"%d", item.number]];
    
    NSData *imageData = UIImagePNGRepresentation(image);
    BOOL succes = [imageData writeToFile:path atomically:NO];
    
    if(succes) {
       // Do nothing for now
    }else {
        [self showDialogWithMessage:@"Unable to save image!" title:@"Message"];
    }
}

// This will be called when the user canceled while trying to change the picture of a cell.
-(void)imageWillCancel {
    // Do nothing for now.
}

- (IBAction)changeImageClick:(UIButton *)sender {
    sender.tag = 0;
}

-(NSIndexPath *) getButtonIndexPath:(UIButton *) button {
    CGRect buttonFrame = [button convertRect:button.bounds toView:self.tableView];
    return [self.tableView indexPathForRowAtPoint:buttonFrame.origin];
}

#pragma mark - Switcher
- (IBAction)switchClick:(UISwitch *)sender {
    NSIndexPath *indexPath = [self getSwitchIndexPath:sender];
    FoodItem *item;
    
    if(self.searchResult.count <= 0) {
        item = self.list[indexPath.row];
        
    }else {
        item = self.searchResult[indexPath.row];
        
    }
    
    item.switchIsActive = !item.switchIsActive;
    if(item.switchIsActive){
        [self.foodItems.selectedItemsForCompare addObject:item];
        
    }else {
        [self.foodItems.selectedItemsForCompare removeObject:item];
        
    }
}

-(NSIndexPath*) getSwitchIndexPath:(UISwitch *) switcher {
    CGRect switchFrame = [switcher convertRect:switcher.bounds toView:self.tableView];
    return [self.tableView indexPathForRowAtPoint:switchFrame.origin];
}
@end
