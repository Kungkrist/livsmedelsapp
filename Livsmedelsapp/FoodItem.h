//
//  FoodItem.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-05.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FoodItem : NSObject
-(instancetype) initWithName:(NSString*) name Number:(int)number;
@property (nonatomic) NSString* name;
@property (nonatomic) int number;
@property (nonatomic) NSDictionary *nutrientValues;
@property (nonatomic) UIImage *image;
@property (nonatomic) BOOL switchIsActive;

- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;
- (float)getHealthyScore;
@end
