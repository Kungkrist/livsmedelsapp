//
//  ItemDetailTableViewCell.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-31.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nutrientName;
@property (weak, nonatomic) IBOutlet UILabel *nutrientValue;

@end
