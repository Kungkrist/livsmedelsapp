//
//  HelpDetailsViewController.m
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-31.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "HelpDetailsViewController.h"

@interface HelpDetailsViewController ()


@end

@implementation HelpDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textView.text = self.text;
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
