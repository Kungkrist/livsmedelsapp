//
//  FoodItem.m
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-05.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "FoodItem.h"

@implementation FoodItem
-(instancetype) initWithName:(NSString *)name Number:(int)number {
    self = [super init];
    
    if(self) {
        self.name = name;
        self.number = number;
        
        UIImage *image = [self lookForImage];
        
        if(image) {
            self.image = image;
            self.switchIsActive = NO;
        }else {
            //NSLog(@"No image was found");
        }
    }
    
    return self;
}

-(float)getHealthyScore {
    if(self.nutrientValues.count <= 0)
        return 0;
    
    NSNumber *fat = self.nutrientValues[@"fat"];
    NSNumber *protein = self.nutrientValues[@"protein"];
    NSNumber *kcal = self.nutrientValues[@"energyKcal"];
    NSNumber *vitaminD = self.nutrientValues[@"vitaminD"];
    NSNumber *vitaminE = self.nutrientValues[@"vitaminE"];
    NSNumber *vitaminC = self.nutrientValues[@"vitaminC"];
    
    float vitaminesValue = [NSNumber numberWithFloat:vitaminD.floatValue + vitaminE.floatValue + vitaminC.floatValue].floatValue;
    float energyValue = [NSNumber numberWithFloat:(fat.floatValue + protein.floatValue) - (kcal.floatValue /2)].floatValue;
    float totalValue = (((energyValue/2) * (vitaminesValue +1)) / 100) +10;
    
    return totalValue;
}

-(UIImage*) lookForImage {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths[0] stringByAppendingString:[NSString stringWithFormat:@"%d", self.number]];
    return [UIImage imageWithContentsOfFile:path];
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.name forKey:@"itemName"];
    [encoder encodeObject:[NSString stringWithFormat:@"%d", self.number] forKey:@"itemNumber"];
    [encoder encodeObject:self.nutrientValues forKey:@"itemNutrientValues"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.name = [decoder decodeObjectForKey:@"itemName"];
        NSString *numberAsString = [decoder decodeObjectForKey:@"itemNumber"];
        self.number = numberAsString.intValue;
        self.nutrientValues = [decoder decodeObjectForKey:@"itemNutrientValues"];
        
        UIImage *image = [self lookForImage];
        
        if(image) {
            self.image = image;
        }else {
            //NSLog(@"No image was found");
        }
    }
    return self;
}

-(NSString*) description {
    return [NSString stringWithFormat:@"Name: %@, Number: %d", self.name, self.number];
}
@end
