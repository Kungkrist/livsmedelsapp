//
//  ChangeImageViewController.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-11.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageHelperProtocol.h"

@interface ChangeImageViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

-(void) setSender:(id<ImageHelper>)sender item:(id)item;

@end
