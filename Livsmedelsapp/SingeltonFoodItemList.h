//
//  SingeltonFoodItemList.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-05.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FoodItem.h"

@interface SingeltonFoodItemList : NSObject
@property (readonly) NSMutableArray *items;
@property (nonatomic) NSMutableArray *savedItems;
@property (nonatomic) NSMutableArray *selectedItemsForCompare;
@property (nonatomic) FoodItem *currentItem;

- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;
-(void)addItem:(FoodItem *)item;
-(void)removeItemAtIndex:(int) index;

+(SingeltonFoodItemList *) getInstance;
@end
