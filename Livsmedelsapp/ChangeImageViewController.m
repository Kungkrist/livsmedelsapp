//
//  ChangeImageViewController.m
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-11.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "ChangeImageViewController.h"
#import "FoodItem.h"

@interface ChangeImageViewController ()
@property (weak, nonatomic) IBOutlet UIButton *changeImageButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelBarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveBarButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic) id<ImageHelper> sender;
@property (nonatomic) id item;
@end

@implementation ChangeImageViewController

// Fixa så att det går att byta bild när man redan har en bild sparad..

- (void)viewDidLoad {
    [super viewDidLoad];
    self.saveBarButton.enabled = NO;
    // Do any additional setup after loading the view.
    
    self.imageView.image = ((FoodItem*)self.item).image;
    [self.view sendSubviewToBack:self.imageView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showDialogWithMessage:(NSString *)message title:(NSString *)title {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* folder = [UIAlertAction actionWithTitle:@"Folder" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self presentViewController:picker animated:YES completion:nil];
        [alert dismissViewControllerAnimated:YES completion:nil];
        }];
    
    UIAlertAction* camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
        [alert addAction:folder];
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        [alert addAction:camera];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)changeImageClick:(UIButton *)sender {
    [self showDialogWithMessage:@"Choose where you want to get the picture from." title:@"Change picture"];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    //UIImagePickerControllerOriginalImage
    UIImage *image = info[UIImagePickerControllerEditedImage];
    self.imageView.image = image;
    self.saveBarButton.enabled = YES;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)saveClick:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.sender imageWillBeChanged:self.imageView.image item:self.item];
}
- (IBAction)cancelClick:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.sender imageWillCancel];
}

-(void) setSender:(id<ImageHelper>)sender item:(id)item {
    self.sender = sender;
    self.item = item;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
