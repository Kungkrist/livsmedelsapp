//
//  CustomTableViewCell.m
//  Livsmedelsapp
//
//  Created by DEA on 2016-02-26.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "CustomTableViewCell.h"

@interface CustomTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *imageView2;
@property (nonatomic) UIColor *startColor;
@end

@implementation CustomTableViewCell

- (void)awakeFromNib {
    [self.contentView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.contentView.layer setBorderWidth:1.0f];
    self.startColor = self.loadingView.backgroundColor;
    self.testLoadingView.color = [UIColor clearColor];
    self.loadingView.backgroundColor = [UIColor clearColor];
}


-(void)stopSpinner {
    [self.testLoadingView stopAnimating];
    self.loadingView.backgroundColor = [UIColor clearColor];
    self.testLoadingView.hidden = YES;
    self.loadingView.userInteractionEnabled = NO;
}

-(void)startSpinner {
    self.testLoadingView.color = [UIColor grayColor];
    [self.testLoadingView startAnimating];
    self.testLoadingView.hidden = NO;
    self.loadingView.backgroundColor = self.startColor;
    self.loadingView.userInteractionEnabled = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if(self.image != nil)
        self.imageView2.image = self.image;
    // Configure the view for the selected state
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
