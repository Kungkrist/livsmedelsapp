//
//  CompareViewController.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-19.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GKBarGraph.h>
#import "FoodItem.h"
#import "SingeltonFoodItemList.h"

@interface CompareViewController : UIViewController <GKBarGraphDataSource, UIPickerViewDelegate, UIPickerViewDataSource>

@end
