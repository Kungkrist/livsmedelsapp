//
//  ImageHelperProtocol.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-11.
//  Copyright © 2016 DEA. All rights reserved.
//

@protocol ImageHelper
-(void) imageWillBeChanged:(UIImage*)image item:(id) item;
-(void) imageWillCancel;

@end
