//
//  ViewController.m
//  Livsmedelsapp
//
//  Created by DEA on 2016-02-23.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *testLabel;
@property (nonatomic) SingeltonFoodItemList *foodItems;
@property (nonatomic) NSMutableArray *nutrientValues;
@property (nonatomic) NSMutableArray *nutrientKeys;
@end

@implementation ViewController

-(SingeltonFoodItemList *) foodItems {
    if(_foodItems == nil)
        _foodItems = [SingeltonFoodItemList getInstance];
    
    return _foodItems;
}
- (IBAction)settingsBarButtonClick:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"ToSettingsIdentifier" sender:sender];
}

-(void) startSpinner {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(screenWidth/2., screenHeight/2);
    spinner.tag = 12;
    [self.view addSubview:spinner];
    [spinner startAnimating];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.testLabel.text = [NSString stringWithFormat:@"%@", self.foodItems.currentItem.name];
    
    self.nutrientValues = [self.foodItems.currentItem.nutrientValues allValues].mutableCopy;
    self.nutrientKeys = [self.foodItems.currentItem.nutrientValues allKeys].mutableCopy;
    [self.tableView reloadData];
    
    if(self.nutrientValues.count == 0 && self.nutrientKeys.count == 0)
      [self startSpinner];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0)
        return self.foodItems.currentItem.nutrientValues.count;
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ItemDetailTableViewCell *cell;
    
    if(indexPath.section == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    }
    
    
    if(self.nutrientKeys.count > 1) {
        cell.nutrientName.text = [NSString stringWithFormat:@"%@", self.nutrientKeys[indexPath.row]];
        cell.nutrientValue.text = [NSString stringWithFormat:@"%@", self.nutrientValues[indexPath.row]];
    }
    
    return cell;
}

@end
