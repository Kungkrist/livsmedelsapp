//
//  CompareViewController.m
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-19.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "CompareViewController.h"

@interface CompareViewController ()
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerWheel;
@property (weak, nonatomic) IBOutlet GKBarGraph *barGraph;
@property (nonatomic) SingeltonFoodItemList *foodItems;
@property (nonatomic) NSArray *barColors;
@property (nonatomic) NSArray *values;
@end

@implementation CompareViewController {
    NSString *currentValue;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.values[row];
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.values.count;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    currentValue =  self.values[row];
  
    [self.barGraph draw];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.values = @[@"healthy score", @"fat", @"protein", @"energyKcal", @"calcium"];
    currentValue = self.values.firstObject;
    
    self.pickerWheel.dataSource = self;
    self.pickerWheel.delegate = self;
    
    self.barColors = @[[UIColor blackColor], [UIColor redColor], [UIColor greenColor], [UIColor blueColor], [UIColor purpleColor]];
    
    self.foodItems = [SingeltonFoodItemList getInstance];
    int count = (int)self.foodItems.selectedItemsForCompare.count;
    int width = self.view.bounds.size.width / 10;
    int height = self.view.bounds.size.height / 20;
    for(int i = 0; i < count; i++) {
        CGRect viewRect = CGRectMake(0, (width * (i+1)) - height, width, width);
        CGRect labelRect = CGRectMake(width + width / 4, (width * (i+1)) - height, width * 10, width);
        
        UILabel *label = [[UILabel alloc] initWithFrame:labelRect];
        label.text = [self.foodItems.selectedItemsForCompare[i] name];
        
        UIView *view = [[UIView alloc] initWithFrame: viewRect];
        view.backgroundColor = self.barColors[i];
        
        [self.infoView addSubview:view];
        [self.infoView addSubview:label];
    }
    
    self.barGraph.dataSource = self;
    [self.barGraph draw];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfBars {
    return self.foodItems.selectedItemsForCompare.count;
}

-(NSNumber *)valueForBarAtIndex:(NSInteger)index {
    FoodItem *item = self.foodItems.selectedItemsForCompare[index];
    if(![currentValue isEqualToString:@"healthy score"])
        return item.nutrientValues[currentValue];
    
    return [NSNumber numberWithFloat:[item getHealthyScore]];
}

-(NSString *)titleForBarAtIndex:(NSInteger)index {
    FoodItem *item = self.foodItems.selectedItemsForCompare[index];
    if(![currentValue isEqualToString:@"healthy score"])
        return [NSString stringWithFormat:@"%@", item.nutrientValues[currentValue]];
    
    return [NSString stringWithFormat:@"%f", [item getHealthyScore]];
    
}

-(UIColor *)colorForBarAtIndex:(NSInteger)index {
    return self.barColors[index];
}

- (IBAction)goBackClick:(UIBarButtonItem *)sender {
    for(FoodItem *item in self.foodItems.selectedItemsForCompare) {
        item.switchIsActive = NO;
    }
    self.foodItems.selectedItemsForCompare = @[].mutableCopy;
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
