//
//  HelpViewController.m
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-16.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController()
@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;

@property(nonatomic) NSArray *myButtons;

@property (nonatomic) JSONHelper *jsonHelper;
@property (nonatomic) SingeltonFoodItemList *foodItems;
@property (weak, nonatomic) IBOutlet UIButton *saveDelButton;
@property (weak, nonatomic) IBOutlet UIButton *changeImageButton;
@property (weak, nonatomic) IBOutlet UIButton *compareButton;

@end

@implementation HelpViewController {
    NSMutableArray *secondTagForButtons;

}

- (IBAction)buttonClick:(UIButton *)sender {
    NSLog(@"Button tag: %d", sender.tag);
}

-(UIDynamicAnimator*)animator {
    if(_animator == nil) {
        _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    }
    
    return _animator;
}

-(void)viewDidLoad {
    secondTagForButtons = @[].mutableCopy;
    self.jsonHelper = [JSONHelper getInstance];
    self.foodItems = [SingeltonFoodItemList getInstance];
    
    self.saveDelButton.backgroundColor = [UIColor redColor];
    self.changeImageButton.backgroundColor = [UIColor grayColor];
    self.compareButton.backgroundColor = [UIColor yellowColor];
    
    
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.saveDelButton, self.changeImageButton, self.compareButton]];
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.saveDelButton, self.changeImageButton, self.compareButton]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
    
    self.myButtons = @[self.saveDelButton, self.changeImageButton, self.compareButton];
    
    int counter = 0;
    for (UIButton *aButton in self.myButtons) {
        aButton.tag = counter;
        [secondTagForButtons addObject:@0];
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        longPress.minimumPressDuration=0.5;
        [aButton addGestureRecognizer:longPress];
        
        counter++;
    }
}

- (void)longPress:(UILongPressGestureRecognizer*)gesture {
    UIButton *button = (UIButton*)gesture.view;
    
    [self.view bringSubviewToFront:button];
    if ( gesture.state == UIGestureRecognizerStateBegan ) {
        if([secondTagForButtons[button.tag] isEqualToNumber:@0]) {
            [UIView animateWithDuration:1.0f delay:0.0 options:kNilOptions animations:^{
                CGAffineTransform scaleTrans  = CGAffineTransformMakeScale(3.0f, 3.0f);
                CGAffineTransform moveTrans = CGAffineTransformMakeTranslation(0, CGRectGetMaxY(button.bounds) *-1);
                
                button.transform = CGAffineTransformConcat(scaleTrans, moveTrans);
            } completion:^(BOOL finished) {
                secondTagForButtons[button.tag] = @1;
            }];
        }else {
            [UIView animateWithDuration:0.5f delay:0.0 options:kNilOptions animations:^{
                button.transform = CGAffineTransformConcat(CGAffineTransformMakeScale(1.0f, 1.0f), CGAffineTransformMakeTranslation(1.0f, 1.0f));
            } completion:^(BOOL finished) {
                secondTagForButtons[button.tag] = @0;
            }];
        }
        
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier  isEqual: @"ToListIdentifier"] && self.foodItems.items.count == 0) {
        ItemsTableViewController *destination = [segue destinationViewController];
        [self.jsonHelper getJsonObjectsWithSender:destination link:@"http://matapi.se/foodstuff?query=" identifier:1];
    } else if([segue.identifier isEqualToString:@"HelpDetailIdentifier"]) {
        NSLog(@"fiskaresfskd");
        UIButton *button = (UIButton*)sender;
        HelpDetailsViewController *destination = [segue destinationViewController];
        NSLog(@"tagge: %ld", (long)button.tag);
        
        if(button.tag == 0) {
            destination.text = @"To save or delete an item, push down your finger and swipe left. When the green or red (depending on witch table you're viewing) appear, tap on it.";
        }else if(button.tag == 1) {
            destination.text = @"If you want to change the image of a specified item, tap on that item's current image (default image is a question mark). You will the be sent to a separate view where you will be able to change the image.";
        }else if(button.tag == 2) {
            NSLog(@"iiiii");
            destination.text = @"All you need to do to compare items is to tap on the scale icon. The icon will then turn green, if you select 2-5 items and then push the scale icon again, then you will be sent to a separate view where the specified items will be compared. \n\nTo select item, tap on the checkbox so it turns black. The checkbox is placed at the right on every cell.";
        }
    }
}

@end
