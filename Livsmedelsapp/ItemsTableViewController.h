//
//  ItemsTableViewController.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-02-26.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSONHelper.h"
#import "JSONPopulatable.h"
#import "SingeltonFoodItemList.h"
#import "FoodItem.h"
#import "ViewController.h"
#import "ChangeImageViewController.h"
#import "ImageHelperProtocol.h"

@interface ItemsTableViewController : UITableViewController <JSONPopulatable, JSONPopulatableWithCell, ImageHelper, UISearchResultsUpdating>

@end
