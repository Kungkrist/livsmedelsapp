//
//  JSONHelper.m
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-05.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "JSONHelper.h"

@implementation JSONHelper

static JSONHelper *instance;

+(JSONHelper *)getInstance {
    if(instance == nil)
        instance = [[JSONHelper alloc]init];
    
    return instance;
}

-(void) getJsonObjectsWithSender:(id<JSONPopulatable>)sender link:(NSString*)link identifier:(int)identifier{
    NSURL *url = [NSURL URLWithString:link];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error) {
            [sender populate:error identifier:-1];
            return;
        }
        
        NSError *jsonParseError = nil;
        
        id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            [sender populate:jsonParseError identifier:-1];
            return;
        }
        [sender populate:result identifier:identifier];
    }];
    
    [task resume];
    
}

-(void) getJsonObjectsWithCell:(id)cell indexPathRow:(int)index sender:(id<JSONPopulatableWithCell>)sender link:(NSString*)link identifier:(int)identifier{
    NSURL *url = [NSURL URLWithString:link];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error) {
            [sender populateWithCell:cell indexPathRow:index result:error identifier:-1];
            return;
        }
        
        NSError *jsonParseError = nil;
        
        id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            [sender populateWithCell:cell indexPathRow:index result:jsonParseError identifier:-1];
            return;
        }
        [sender populateWithCell:cell indexPathRow:index result:result identifier:identifier];
    }];
    
    [task resume];
    
}

@end
