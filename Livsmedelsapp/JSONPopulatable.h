//
//  JSONPopulatable.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-05.
//  Copyright © 2016 DEA. All rights reserved.
//

@protocol JSONPopulatable
-(void) populate:(id)result identifier:(int)identifier;

@end

@protocol JSONPopulatableWithCell

-(void) populateWithCell:(id)cell indexPathRow:(int)index result:(id)result identifier:(int)identifier;

@end
