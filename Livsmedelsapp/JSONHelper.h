//
//  JSONHelper.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-05.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONPopulatable.h"

@interface JSONHelper : NSObject
@property (nonatomic, strong) NSArray *allItems;

+(JSONHelper *) getInstance;
-(void) getJsonObjectsWithSender:(id<JSONPopulatable>)sender link:(NSString*)link identifier:(int)identifier;
-(void) getJsonObjectsWithCell:(id)cell indexPathRow:(int)index sender:(id<JSONPopulatableWithCell>)sender link:(NSString*)link identifier:(int)identifier;
@end
