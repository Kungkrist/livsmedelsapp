//
//  ViewController.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-02-23.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodItem.h"
#import "JSONPopulatable.h"
#import "SingeltonFoodItemList.h"
#import "ItemDetailTableViewCell.h"

@interface ViewController : UIViewController
@property (nonatomic) FoodItem *item;
@property (weak, nonatomic) IBOutlet UILabel *ostText;

@end

