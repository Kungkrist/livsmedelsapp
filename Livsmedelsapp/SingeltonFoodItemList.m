//
//  SingeltonFoodItemList.m
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-05.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "SingeltonFoodItemList.h"

@implementation SingeltonFoodItemList

@synthesize items = _items;
static SingeltonFoodItemList *instance;

+(SingeltonFoodItemList*) getInstance {
    if(instance == nil)
        instance = [[SingeltonFoodItemList alloc] init];
    
    return instance;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.savedItems forKey:@"savedItems"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.savedItems = [decoder decodeObjectForKey:@"savedItems"];
    }
    return self;
}

-(NSMutableArray*)selectedItemsForCompare {
    if(_selectedItemsForCompare == nil)
        _selectedItemsForCompare = @[].mutableCopy;
    
    return _selectedItemsForCompare;
}

-(NSMutableArray*)savedItems {
    if(_savedItems == nil)
        _savedItems = @[].mutableCopy;
    
    return _savedItems;
}

-(NSMutableArray*)items {
    if(_items == nil)
        _items = @[].mutableCopy;
    
    return _items;
}

-(void)addItem:(FoodItem*)item {
    [_items addObject:item];
}

-(void)removeItemAtIndex:(int) index {
    [_items removeObjectAtIndex:index];
}
@end
