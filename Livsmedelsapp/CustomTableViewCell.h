//
//  CustomTableViewCell.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-02-26.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageHelperProtocol.h"

@interface CustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *compareSwitch;
@property (weak, nonatomic) IBOutlet UIView *saveButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *testLoadingView;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UILabel *itemName;
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet UILabel *protainLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (nonatomic) UIImage *image;

-(void)stopSpinner;
-(void)startSpinner;
@end
