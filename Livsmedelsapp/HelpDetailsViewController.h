//
//  HelpDetailsViewController.h
//  Livsmedelsapp
//
//  Created by DEA on 2016-03-31.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (nonatomic) NSString *text;
@end
